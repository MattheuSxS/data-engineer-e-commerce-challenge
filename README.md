# Desafio de Engenharia de Dados: Pipeline de Dados para Análise de E-commerce

Para este desafio, vamos usar o dataset "E-commerce Events History in Cosmetics Shop" do Kaggle. Este conjunto de dados contém informações de eventos de um site de comércio eletrônico de cosméticos coletados durante um ano.

Regras do desafio:

-   `Arquitetura e Armazenamento de Dados`: Armazene os dados no BigQuery, um banco de dados relacional gerenciado pela Google Cloud. Inclua um diagrama da arquitetura do pipeline de dados para este projeto.

-   `Linguagens de Programação`: Use Python para limpar, manipular e transformar os dados. Utilize SQL para extrair insights dos dados armazenados.

-   `ETL`: Use o Google Cloud Dataflow para criar pipelines de dados ETL que irão ingerir, limpar e transformar os dados antes de armazená-los no BigQuery.

-   `Controle de Versão`: Use o GitHub para controle de versão do seu código. O repositório deve conter todos os scripts Python e SQL, juntamente com quaisquer notebooks Jupyter que você possa ter usado para explorar os dados. Inclua também o arquivo README.md descrevendo o objetivo do projeto, a arquitetura do sistema, como executar o código e os insights extraídos dos dados.

Tarefas do Desafio:

1.   Configurar o ambiente Google Cloud e carregar o conjunto de dados no BigQuery.

2. Escrever scripts Python para limpar e transformar os dados. Por exemplo, você pode precisar lidar com valores nulos, duplicados, erros de formatação etc.

3. Usar o Google Cloud Dataflow para criar pipelines de dados que executam estes scripts Python e carregam os dados transformados no BigQuery.

4. Escrever consultas SQL para extrair insights dos dados. Por exemplo, você pode querer saber os produtos mais vendidos, a hora do dia em que a maioria das compras é feita, ou o valor médio de compra por usuário.

5. Adicionar todos os códigos e documentação no GitHub para controle de versão.

6. Criar um relatório final apresentando a arquitetura do sistema, como os dados foram limpos e transformados, as consultas SQL usadas para extrair insights, e os insights que você descobriu.

Este é um desafio complexo que requer habilidades em engenharia de dados, SQL, Python e controle de versão. É um grande projeto para demonstrar suas habilidades técnicas e também sua habilidade em comunicar resultados técnicos. Boa sorte!