import polars as pl


FILE_PATH = '../data/raw/2019-Dec.csv'


df = pl.read_csv(source=FILE_PATH, separator=',', has_header=True, encoding='utf8')

print(df.head())